/*
Author: Benoit d'Aramon
Date: 2/7/2018
Comment: Read analog input and output PWM
*/
#include <Servo.h>

Servo fan;
int const potPin = A0;
int potVal;
int angle;

void setup() {
	fan.attach(3, 1000, 2000);
	fan.write(0);
}

void loop() {
	potVal = analogRead(potPin);
	angle = map(potVal, 0, 1023, 0, 179);
	fan.write(angle);
	delay(100);
}

